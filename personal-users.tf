##################################################
############## Analysis users output #########
##################################################

module "personal-users" {
  source = "git@gitlab.com:sonata1/pagerduty.git"
   users = var.personal-users
}


##################################################
############## Analysis  users output #########
##################################################
output "personal-users" {
 value = module.personal-users.users
}
