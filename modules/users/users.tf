resource  "pagerduty_user" "new_user" {
  for_each = {for i, v in var.users:  i => v}

  name  = each.value.username
  email = each.value.useremail
  role  = each.value.userrole
  job_title = each.value.usertitle
  time_zone = each.value.time_zone
	description = each.value.description 
  
}
