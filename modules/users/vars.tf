variable "users" {
    type = map(object({
        username       = string
        useremail      = string
        userrole       = string
        usertitle      = string
        time_zone      = any
        description    = any
      }))
    }
