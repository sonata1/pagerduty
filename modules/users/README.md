## _General_ : 
This module is used to create Pagerduty users. 

## _Prerequisite_: 
This module needs Terraform 0.13.0 or newer.
You can download the latest Terraform version from here(https://www.terraform.io/downloads.html).
This module deploys Pagerduty users in respective feature branches.

## _Features_:
Below we are able to check the resources that are being created as part of this module call:

**Pagerduty User** 
- 	Pagerduty User Name
- 	Pagerduty User Title
- 	Pagerduty User ROle

## _Usage_ :
To use this module, add the following call to your code:

**Example Usage**

  ```
module "enabler-users"{
   source = "git@ssh.source.tui:incident-management/pagerduty/pagerduty-modules.git//newusers?ref=v12.0.0"
   users = var.enabler-users
  }

  pagerduty = {
  source = "Pagerduty/pagerduty"
  }
```

 


    ##################################################
    ############## Enabler users creation #########
    ##################################################

    variable "enabler-users" {
      default= {
            "swetha" = {
                username  = "Swetha Thammineni",
                useremail = "swetha.thammineni@tui.co.uk",
                userrole  = "user",
                usertitle = "Cloud Engineer",
                time_zone = "",
	            description = "" 
        },
         "ashish" = {
                username  = "Ashish Niranjan",
                useremail = "ashish.niranjan@tui.co.uk",
                userrole  = "user",
                usertitle = "Cloud Engineer",
                time_zone = "",
	            description = "" 
				}
				
	    }
    }

## _Argument Referance_
The following arguments are supported:

- **name** - (Required) The name of the user.
- **email** - (Required) The user's email address.
- **role**- (Optional) The user role. Can be admin, limited_user, observer, owner, read_only_user, read_only_limited_user, restricted_access, or user.
- **job_title**- (Optional) The user's title.
- **time_zone** - (Optional) The time zone of the user. Default is account default timezone.
- **description** - (Optional) A human-friendly description of the user. If not set, a placeholder of "Managed by Terraform" will be set.



## _Usage_
module.<module_name>

**Output values:**
- User_email
- User_ID


**Output Usage:**
```
data "pagerduty_user" "get_user_id" {
  email = var.email
}

data "pagerduty_team" "get_team_id" {
  name = var.team
}
```


## _Authors_ :
Module maintained by the  - Cloud Enablers
