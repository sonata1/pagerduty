output "users" {
  value = tomap({
    for new_user in pagerduty_user.new_user : new_user.id => new_user.email
  })
}