##################################################
############## Personal users creation #########
##################################################

variable "personal-users" {
      default= {
            "niroop" = {
                username  = "niroop kumar",
                useremail = "niroopkumar.tallaka@gmail.com",
                userrole  = "user",
                usertitle = "Technology Team Lead",
                time_zone = "",
	            description = "" 
        },
        "nirooptgd" = {
                username  = "niroop tgd",
                useremail = "niroop.tgd@gmail.com",
                userrole  = "user",
                usertitle = "Technology Lead",
                time_zone = "",
	            description = "" 
        },
        "nirooptgd" = {
                username  = "niroop kumar",
                useremail = "niroopkumar.m@gmail.com",
                userrole  = "user",
                usertitle = "Technology Lead",
                time_zone = "",
	            description = "" 
        }
      }
}